using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Globalization;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController: ControllerBase 
    {
        [HttpPost("CadastrarVenda")]
        public IActionResult CadastrarVenda(string Itens, int IdVendedor){

            if (Itens == "" || Itens == null)
                return NotFound("A venda não pode ser cadastrada sem nenhum item");
            venda novaVenda = new venda(Itens, IdVendedor);

            // Rotina de consular vendedor
            List<vendedor> Vendedores = new List<vendedor>();
            List<venda> Vendas = new List<venda>();
            string listaVendas = new string("");
            if (System.IO.File.Exists("Arquivos/vendedores.json")){
                Vendedores = JsonConvert.DeserializeObject<List<vendedor>>(System.IO.File.ReadAllText("Arquivos/vendedores.json"));
                try{
                    foreach (vendedor v in Vendedores){
                        if (v.id == novaVenda.Vendedor.id){
                            // Se o vendedor existe realiza o cadastro do mesmo na venda:
                            novaVenda.Vendedor = v;
                            if (System.IO.File.Exists("Arquivos/vendas.json")){
                                Vendas = JsonConvert.DeserializeObject<List<venda>>(System.IO.File.ReadAllText("Arquivos/vendas.json"));
                                novaVenda.Id = Vendas.Count+1;
                                Vendas.Add(novaVenda);
                                listaVendas = JsonConvert.SerializeObject(Vendas, Formatting.Indented);
                                System.IO.File.WriteAllText("Arquivos/vendas.json",listaVendas);
                            }
                            else{
                                novaVenda.Id = Vendas.Count+1;
                                Vendas.Add(novaVenda);
                                listaVendas = JsonConvert.SerializeObject(Vendas, Formatting.Indented);
                                System.IO.File.WriteAllText("Arquivos/vendas.json",listaVendas);
                            }

                        }
                    }
                }
                catch{
                    return NotFound("Não existe vendedor cadastrado com esse ID!");
                }
            }
            else{
                return NotFound("Não existe nenhum vendedor cadastrado!");
            }
            if (novaVenda.Vendedor.cpf=="" && novaVenda.Vendedor.nome==""){
                return NotFound("Não existe vendedor cadastrado com esse ID!");
            }
            else{
                return Ok(novaVenda);
            }
        }
        
        [HttpGet("BuscaVendaPorID/{Id}")]
        public IActionResult BuscaVendaPorID(int Id){
            venda buscaVenda = new venda("", 0);
            buscaVenda.Id = Id;
            List<venda> Vendas = new List<venda>();
            if (System.IO.File.Exists("Arquivos/vendas.json")){
                Vendas = JsonConvert.DeserializeObject<List<venda>>(System.IO.File.ReadAllText("Arquivos/vendas.json"));
                try{
                    foreach (venda v in Vendas){
                        if (v.Id == Id){
                            buscaVenda = v;
                        }
                    }
                }
                catch{
                    return NotFound("Não existe venda cadastrada com esse ID!");
                }
            }
            else{
                return NotFound("Não existe nenhuma venda cadastrada!");
            }
            if (buscaVenda.Id == 0){
                return NotFound("Não existe venda cadastrada com esse ID!");
            }
            else{
                return Ok(buscaVenda);
            }

        }
    
        [HttpPut("AtualizarVenda/{Id}")]
        public IActionResult AtualizarVenda(int Id, string Status){
            venda buscaVenda = new venda("", 0);
            buscaVenda.Id = Id;
            List<venda> Vendas = new List<venda>();
            if (System.IO.File.Exists("Arquivos/vendas.json")){
                Vendas = JsonConvert.DeserializeObject<List<venda>>(System.IO.File.ReadAllText("Arquivos/vendas.json"));
                try{
                    foreach (venda v in Vendas){
                        if (v.Id == Id){
                            if ((v.status == "Aguardando Pagamento") && (Status == "Pagamento Aprovado" || Status == "Cancelado") ){
                                v.status = Status;
                                buscaVenda = v;
                            }
                            else if ((v.status == "Pagamento Aprovado") && (Status == "Enviado a Transportadora" || Status == "Cancelado")){
                                v.status = Status;
                                buscaVenda = v;
                            }
                            else if (v.status == "Enviado a Transportadora" && Status == "Pedido Entregue"){
                                v.status = Status;
                                buscaVenda = v;
                            }
                            else if (v.status == "Cancelado" || Status == "Pedido Entregue"){
                                return NotFound("Pedidos Entregues ou cancelados não podem mais ser alterados!");
                            }
                            else{
                                return NotFound("O tipo de alteração solicitado não é possível!");
                            }

                        }
                    }
                    string listaVendas = new string("");
                    listaVendas = JsonConvert.SerializeObject(Vendas, Formatting.Indented);
                    System.IO.File.WriteAllText("Arquivos/vendas.json",listaVendas);
                }
                catch{
                    return NotFound("Não existe venda cadastrada com esse ID!");
                }
            }
            else{
                return NotFound("Não existe nenhuma venda cadastrada!");
            }
            if (buscaVenda.Id == 0){
                return NotFound("Não existe venda cadastrado com esse ID!");
            }
            else{
                return Ok(buscaVenda);
            }
            

        }
    
    }
}