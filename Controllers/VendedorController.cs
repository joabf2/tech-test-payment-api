using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Globalization;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController: ControllerBase 
    {
        [HttpPost("CadastrarVendedor")]
        public IActionResult CadastrarVendedor(string Cpf, string Nome, string Email, string Telefone){
            vendedor novoVendedor = new vendedor(Cpf, Nome, Email, Telefone);
            List<vendedor> Vendedores = new List<vendedor>();
            string listaVendedores = new string("");
            if (System.IO.File.Exists("Arquivos/vendedores.json")){
                Vendedores = JsonConvert.DeserializeObject<List<vendedor>>(System.IO.File.ReadAllText("Arquivos/vendedores.json"));
                novoVendedor.id = Vendedores.Count+1;
                Vendedores.Add(novoVendedor);
                listaVendedores = JsonConvert.SerializeObject(Vendedores, Formatting.Indented);
                System.IO.File.WriteAllText("Arquivos/vendedores.json",listaVendedores);
            }
            else{
                novoVendedor.id = Vendedores.Count+1;
                Vendedores.Add(novoVendedor);
                listaVendedores = JsonConvert.SerializeObject(Vendedores, Formatting.Indented);
                System.IO.File.WriteAllText("Arquivos/vendedores.json",listaVendedores);
            }
            return Ok(novoVendedor);
        }

        [HttpGet("{Id}")]
        public IActionResult BuscaVendedorPorID(int Id){
            vendedor buscaVendedor = new vendedor("", "", "", "");
            buscaVendedor.id = Id;
            List<vendedor> Vendedores = new List<vendedor>();
            if (System.IO.File.Exists("Arquivos/vendedores.json")){
                Vendedores = JsonConvert.DeserializeObject<List<vendedor>>(System.IO.File.ReadAllText("Arquivos/vendedores.json"));
                try{
                    foreach (vendedor v in Vendedores){
                        if (v.id == Id){
                            buscaVendedor = v;
                        }
                    }
                }
                catch{
                    return NotFound("Não existe vendedor cadastrado com esse ID!");
                }
            }
            else{
                return NotFound("Não existe nenhum vendedor cadastrado!");
            }
            if (buscaVendedor.cpf=="" && buscaVendedor.nome==""){
                return NotFound("Não existe vendedor cadastrado com esse ID!");
            }
            else{
                return Ok(buscaVendedor);
            }

        }
        
    }
}