using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using tech_test_payment_api.Controllers;

namespace tech_test_payment_api.Models
{
    public class venda
    {
        public int Id { get; set; }
        public string Itens { get; set; }
        public DateTime Data { get; set; }
        public vendedor Vendedor { get; set; }
        public string status { get; set; }

        public venda (string itens, int IdVendedor){
            this.Id = 0;
            this.Itens = itens;
            this.Data = DateTime.Now;
            this.Vendedor = new vendedor("","","","");
            Vendedor.id = IdVendedor;
            this.status = "Aguardando Pagamento";
        }


    }
}