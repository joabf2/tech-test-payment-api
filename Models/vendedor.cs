using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class vendedor
    {

        public int id { get; set; }
        public string cpf { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }


        public vendedor(string cpf, string nome, string email, string telefone){
            this.cpf = cpf;
            this.nome = nome;
            this.email = email;
            this.telefone = telefone;
        }


        
    }
}